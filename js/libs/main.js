$(document).ready(function(){

   var userCheckbox; // used in userTable functions 

   initAdvancedSearch();
   toggleMenu();
   initToggleUsers();

   // Menu Toggle Script
   function toggleMenu(){
      $(".menu-toggle").on('click', function(e) {
         e.preventDefault();
         $("#wrapper").toggleClass("toggled");
      });
   }

   // Datepicker
   function addDatepicker(){
      $('.datepicker').datepicker({
         format: 'mm/dd/yyyy',
         startDate: '-3d'
      });
   } 

   // Check and uncheck all users
   function getUserCheckbox() { 
      if(userCheckbox == null || userCheckbox == undefined) { 
         fillUserCheckbox(); 
      }
      return userCheckbox;
   }

   function fillUserCheckbox() { 
      userCheckbox = $("input:checkbox").filter(".user");
   }

   function initToggleUsers(){
      toggleSingleUsers();
      toggleAllUsers();
   }

   function toggleSingleUsers(){
      var userCheckbox = getUserCheckbox();

      userCheckbox.each(function(key, checkbox) {
         $(checkbox).on('change', function() {
            var parent = $(this).parent().parent();
            $(parent).toggleClass('row-selected');
            toggleFilters();
         });
      });
   }

   function toggleAllUsers(){
      var userCheckbox = getUserCheckbox();
      var selectAll = $("#checkAll");

      selectAll.on('change', function () {   
         userCheckbox.prop('checked', $(this).prop("checked"));
         userCheckbox.each(function(key, checkbox) {
            var parent = $(this).parent().parent();

            if(userCheckbox.prop('checked')){
               $(parent).addClass('row-selected');  
            } else {
               $(parent).removeClass('row-selected');
            }
         });
         toggleFilters();
      });
   }

   function toggleFilters(){
      var oneChecked = $('input[class=user]:checked').length > 0;
      var userButtonsTableLeft = $('.buttons-user-table-left');

      if(oneChecked) {
         userButtonsTableLeft.addClass('buttons-user-table-left-show');
      } else {
         userButtonsTableLeft.removeClass('buttons-user-table-left-show');
      }
   }

   // Advanced search component
   var firstSelection = '<div class="form-group pull-left"><select class="selectpicker initial-filter"><option disabled selected>Choose a filter</option><option>Comments</option><option>Registration Date</option><option>User Group</option></select><button class="btn btn-danger btn-delete-row">x</button> </div>';
   var commentsOptions = '<option>Is greater than</option><option>Is smaller than</option><option>Is equal to</option>';

   function initAdvancedSearch() {
      addDatepicker();
      addRow();
      removeRow();
      filterOnChangeListener();
      removeFilterTag();
      clearAllFilterTags();
   }

   function addRow() {
      $('#add-row').on('click', function(){
         addFilter($(this).parent());
      });
   }

   function removeRow(){
      $('.selectpicker').selectpicker('refresh');
      $('.btn-delete-row').on('click', function(){
         $(this).closest('.row').remove();
      });
   }

   function addFilter(element) {
      $('<div class="row">'+ firstSelection +'</div>').insertBefore(element);
      $('.selectpicker').selectpicker('refresh');

      filterOnChangeListener();
      removeRow();
   }

   function filterOnChangeListener() {
      $('div.initial-filter').on('change', function() {
         var selectedValue = $(this).find("option:selected").text();
         enableOptions(selectedValue, $(this).parent().parent());
      });
   }

   function enableOptions(selectedValue, row) {
      var secondFilter = row.find('div.second-filter, .second-filter button');
      var selectSecondFilter = secondFilter.siblings('select');
      var thirdFilter = row.find('div.third-filter');
      var inputThirdFilter = thirdFilter.find('input');
      
      if(selectedValue != "Choose a filter") {
         secondFilter.removeClass('disabled');
         selectSecondFilter.prop("disabled", false);
         thirdFilter.removeClass('disabled');
         inputThirdFilter.prop("disabled", false);

        setValuesForOptions(selectedValue, row);

      } else {
         secondFilter.addClass('disabled');
         selectSecondFilter.prop("disabled", true);
         thirdFilter.addClass('disabled');
         inputThirdFilter.prop("disabled", true);
      }
   }

   function setValuesForOptions(selectedValue, row){
      addFilterTag(selectedValue);

      if (selectedValue == "Comments") {
            row.find('select.second-filter').append(commentsOptions);
         } else if(selectedValue == "Registration Date"){
            console.log('registration date');
         } else {
            console.log("user group");
         }
      }

   function addFilterTag(selectedValue){
      var filterTag = '<button class="btn tag">' + selectedValue + '</button>';
      var tags = $('.tag-labels button:last');
      tags.before(filterTag);
   }

   function clearAllFilterTags(){
      $('.tag-labels').on('click','#btn-clear-all', function() {
         var filterTags =  $('.tag-labels').find('.tag');
         filterTags.remove();
      });
   }

   function removeFilterTag(){

      // var filterList = $('div.initial-filter').find("option:selected");

      $('.tag-labels').on('click', 'button.tag', function() {
      //    var tagName = ($(this).text());

      //    filterList.each(function(){
      //       console.log($(this).text());
      //       if(tagName == value.text) {
      //       }
      //    })         
         $(this).remove();
      });
   }
});
