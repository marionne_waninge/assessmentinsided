module.exports = function(grunt) {

// Project configuration.
grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),

  compass: {               
    dist: {                  
      options: {              
        sassDir: 'sass',
        cssDir: 'css'
      }  
    }
  },
  gitcommit: {
      dist: {
          options: {
              message: 'Auto commit',
              allowEmpty: true
          },
          files: [
              {
                  src: ['./'],
                  expand: true
              }
          ]
      }
  },
  gitpull: {
      dist: {}
  },
  gitpush: {
      dist: {}
  },
  watch: {
      css: {
          files: ['sass/*.scss'],
          tasks: ['compass']
      }
  }
});
  
// Load the Grunt plugins.
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-compass');
grunt.loadNpmTasks('grunt-git');

// Register the default tasks.
grunt.registerTask('default', ['watch']);
};